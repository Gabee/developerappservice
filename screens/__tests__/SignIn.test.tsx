import React from 'react'
import renderer from 'react-test-renderer'

import SignIn from '../SignIn';

// Todo: Look into react-native-testing-library for more thorough tests to use for components.
describe(`SignIn Test`, () => {
    it(`has 1 child`, () => {
        const tree = renderer.create(<SignIn testCall={undefined}/>).toJSON();
        expect(tree.children.length).toBe(1);
    });

    it(`renders using Snapshot`, () => {
        const tree = renderer.create(<SignIn />).toJSON();
        expect(tree).toMatchSnapshot();
    })
})