import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, ImageBackground, TextInput, TouchableOpacity } from 'react-native';

import Textbox from '../components/Textbox';
import Button from '../components/Button';

interface SignInProps {
    testCall?: Function
}

const SignIn: React.FC<SignInProps> = (props) => {

    return (
        <SafeAreaView style={styles.container}>
            <ImageBackground
                source={require('../assets/Images/collab.jpg')}
                style={{ height: 741, width: 1112, alignItems: 'center', flex: 1, flexDirection: 'column', justifyContent: 'space-around' }}
            >
                <View style={styles.portalOverlay}>
                    <Text style={{ fontFamily: 'Noteworthy', color: '#fff', fontSize: 86, paddingBottom: 30 }}>
                        Portal
              </Text>
                    <Textbox password={false} text="Email"></Textbox>
                    <Textbox password={true} text="Password new"></Textbox>
                    <Button text="Sign In"></Button>
                    <TouchableOpacity style={{ padding: 10 }}>
                        <Text style={{ color: 'white' }}>
                            Create an account
                </Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </SafeAreaView>
    );
}

export default SignIn;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    portalOverlay: {
        flex: 1,
        backgroundColor: 'rgba(177, 174, 104, 0.54)',
        width: 325,
        height: 100,
        marginTop: 75,
        marginBottom: 100,
        borderRadius: 22,
        alignContent: 'center',
        alignItems: 'center',
    },
});