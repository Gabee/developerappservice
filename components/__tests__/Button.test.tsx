import React from 'react';
import renderer from 'react-test-renderer';

import Button from '../Button'

describe('Button working', () => {
    it('has 1 child', () => {
        const tree = renderer.create(<Button text="default" />).toJSON();
        expect(tree.children.length).toBe(1);
    });

    it('renders using Snapshot', () => {
        const tree = renderer.create(<Button text="default" />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
