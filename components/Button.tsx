import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet} from 'react-native';

interface ButtonProps {
    onPress?: Function,
    text: string,
    style?: Object,

}

const Button: React.FC<ButtonProps> = (props) => {
    return (
        <TouchableOpacity>
            <View style={(props.style ? props.style : styles.buttonStyle)}>
                <Text onPress={() => {props.onPress}} style={{ color: '#fff' }}>
                    Sign in
                  </Text>
            </View>
        </TouchableOpacity>
    )
}

export default Button;

const styles = StyleSheet.create({
    buttonStyle: {
        borderRadius: 22,
        backgroundColor: 'teal',
        padding: 10, 
    }
})