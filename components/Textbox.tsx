import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

interface TextboxProps {
    style?: Object,
    password: Boolean,
    text: string,
}

// Todo: Maybe update styles by making it more customizable for nested native components.
const Textbox: React.FC<TextboxProps> = (props) => {
    return (
        <View style={(props.style ? props.style : styles.textInputStyle)}>
            <TextInput autoCapitalize='none' secureTextEntry={true} placeholder={props.text} style={{ height: 10, alignItems: 'center', color: 'black' }}></TextInput>
        </View>
    )
}

const styles = StyleSheet.create({
    textInputStyle: {
        backgroundColor: '#fff',
        borderRadius: 18,
        height: 36,
        width: 200,
        padding: 15,
        alignItems: 'center',
        marginBottom: 50,
    }
})

export default Textbox;